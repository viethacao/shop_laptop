<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\slide;
use App\User;
use App\products;
use App\customers;
use App\category;
use App\bill;
use App\bill_detail;
use App\comment;
use Validator;
use Hash;
use DB;
use Auth;
use PDF;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
   
    public function index() {   
        $cate = category::all()->count();
        $pro = products::all()->count();
        $cus = customers::all()->count();
        $bill = bill::all()->count();
        $cmt = comment::all()->count();
        $detail = bill_detail::all()->count();
        $slide = slide::all()->count();
        $pro_qty = products::select('quantity')->get();
        $sum = 0;
        foreach ($pro_qty as $value) {
            $a = $value->quantity;
            $sum = $a + $sum;
            
        }
        $detail_qty = bill_detail::select('quantity')->get();
        $sum2 = 0;
        foreach ($detail_qty as $value) {
            $a = $value->quantity;
            $sum2 = $a + $sum2;
        }
        $store = $sum - $sum2;
        
        return view('admin.pages.home', compact('cate','pro','cus','bill','cmt','detail','slide','sum', 'sum2', 'store')); 
    }

    public function getError() {
        return view('error');
    }


    public function getCate() {
        if(Auth::user()->position == "Nhân viên") {
            Auth::logout();
            return redirect()->to('admin/login');
        } else {
            $cate = category::paginate(5);
            $cate_count = category::all()->count();
            $stt = 1;
            return view('admin.pages.category.category', compact('cate', 'stt', 'cate_count'));
        }
        
    }

    public function getAddCate() {
        if(Auth::user()->position == "Nhân viên") {
            Auth::logout();
            return redirect()->to('admin/login');
        } else {
            return view('admin.pages.category.add');
        }
        
    }

    public function getEditCate($id) {
        if(Auth::user()->position == "Nhân viên") {
            Auth::logout();
            return redirect()->to('admin/login');
        }
        $cate = category::find($id);
        if(!$cate) {
            return view('error');
        } 
        return view('admin.pages.category.edit', compact('cate'));
    }


    public function postCate(request $req) {
        $cate = new category;
        $messages = [
		    'required' => ':Attribute require',
        ];
        
        $validator = Validator::make($req->all(), [
            'name'     => 'required',
        ], $messages);

        switch ($req->input('action')) {
            case 'add':
                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator);
                } else {
                    $cate->active = $req->active;
                    $cate->name = $req->name;
                    $cate->save();
                    return redirect('admin/category')->with('success','Thêm mới thành công');
                }
                break;
            case 'cancel':
                return redirect('admin/category');
                break;
        }
    }


    public function updateCate(Request $req, $id) {
        $cate = category::find($id);

        $messages = [
		    'required' => ':Attribute require',
        ];
        
        $validator = Validator::make($req->all(), [
            'name'     => 'required',
        ], $messages);

        switch ($req->input('action')) {
            case 'edit':
                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator);
                } else {
                    $cate->name = $req ->name;
                    $cate->active = $req->active;
                    $cate->save();
                    return redirect('admin/category')->with('success','cập nhật thành công');
                }
                break;
            case 'cancel':
                return redirect('admin/category');
                break;
        }
    }

    public function deleteCate($id) {
        $cate = category::find($id);
        $cate->delete();
        return redirect() -> back(); 
    }






    // slide
    public function getSlide() {
        if(Auth::user()->position == "Nhân viên") {
            Auth::logout();
            return redirect()->to('admin/login');
        } else {
            $slide = slide::paginate(5);
            $slide_count = slide::all()->count();
            $stt = 1;
            return view('admin.pages.slide.slide', compact('slide', 'slide_count', 'stt'));
        }
        
    }

    public function getAddSlide() {
        if(Auth::user()->position == "Nhân viên") {
            Auth::logout();
            return redirect()->to('admin/login');
        } else {
            return view('admin.pages.slide.add');
        }
        
    }

    public function getEditSlide($id) {
        if(Auth::user()->position == "Nhân viên") {
            Auth::logout();
            return redirect()->to('admin/login');
        } else {
            $slide = slide::find($id);
            if(!$slide) {
                return view('error');
            } 
            return view('admin.pages.slide.edit', compact('slide'));
        }
       
    }

    public function postSlide(request $req) {
        $slide = new slide;

        $messages = [
		    'required' => ':Attribute require',
            'mimes'  => 'File is not valid',
        ];
        
        $validator = Validator::make($req->all(), [
            'image' => 'required| mimes:jpeg,jpg,png,gif',
            'link'     => 'required',
        ], $messages);


        switch ($req->input('action')) {
            case 'add':
                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator);
                } else {
                    $file = $req->file('image');
                    $file_name = $file -> getClientOriginalName('image');
                    $file->move('upload/slide', $file_name);
                    $slide->image = $file_name;
                    $slide->active = $req->active;
                    $slide->link = $req->link;
                    $slide->save();
                    return redirect('admin/slide')->with('success','cập nhật thành công');
                }
                break;

            case 'cancel':
                return redirect('admin/slide');
                break;
        }
    }

    public function updateSlide(Request $req, $id) {
        $slide = slide::find($id);

        $messages = [
		    'required' => ':Attribute require',
            'mimes'  => 'File is not valid',
        ];
        
        $validator = Validator::make($req->all(), [
            'image' => 'mimes:jpeg,jpg,png,gif',
            'link'     => 'required',
        ], $messages);

        switch ($req->input('action')) {
            case 'edit':
                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator);
                } else {
                    if( $req -> hasFile('image')) {
                        $file = $req->file('image');
                        $file_name = $file -> getClientOriginalName('image');
                        $file->move('upload/slide', $file_name);
                        $slide->image = $file_name;
                        $slide->active = $req->active;
                        $slide->link = $req->link;
                        $slide->save();
                        return redirect('admin/slide')->with('success','cập nhật thành công');
                    } else {
                        $slide ->image =  $slide ->image;
                        $slide->link = $req ->link;
                        $slide->active = $req->active;
                        $slide->save();
                        return redirect('admin/slide')->with('success','cập nhật thành công');
                    }
                }
                break;

            case 'cancel':
                return redirect('admin/slide');
                break;
        }
    }

    public function deleteSlide($id) {
        $slide = slide::find($id);
        $slide->delete();
        return redirect() -> back(); 
    }

    








    // products
    public function getProduct() {
        if(Auth::user()->position == "Nhân viên") {
            Auth::logout();
            return redirect()->to('admin/login');
        } else {
            $pro = products::paginate(5);
            $pro_count = products::all()->count();
            $stt = 1;
            $cate = category::all();
            return view('admin.pages.products.products', compact('pro', 'stt', 'cate', 'pro_count'));
        }
    }


    public function getAjax(Request $request, $type){
        switch ($type) {
            case 'products':
                return Datatables::of(products::query())
                ->addColumn('manipulation', function ($val) {
                    return '
                    <a href="admin/product/edit/'.$val->id.'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Xem chi tiết">
                    <i class="fa fa-book"></i></a>
                    
                    <a href="admin/product/edit/'.$val->id.'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Chỉnh sửa">
                    <i class="la la-edit"></i></a>
                    <a id="del-pro-'.$val->id.'" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Xóa">
                        <i class="la la-trash"></i>
                    </a>';
                })
                ->editColumn('id', function ($val) {
                    return '<a>'.$val->id.'</a>';
                })
                ->editColumn('name', function ($val) {
                    return '<a>'.$val->name.'</a>';
                })
            
                ->editColumn('image', function ($val) {
                    return '<img style="width: 100px; height: 80px" src="upload/product/'.$val->image.'" />';
                })
                
                ->editColumn('unit_price', function ($val) {
                    return '<a>'.number_format($val->unit_price).'</a>';
                })
                
                ->editColumn('quantity', function ($val) {
                    return '<a>'.$val->quantity.'</a>';
                })
                ->editColumn('active', function ($val) {
                    return '<a>'.$val->active.'</a>';
                })
                ->rawColumns(['manipulation','quantity','id','name','image','unit_price', 'promotion_price', 'active'])
                ->make(true);
                break;

            case 'bills':
                return Datatables::of(bill::query())
                ->addColumn('manipulation', function ($val) {
                    return '
                    <a href="admin/bill-details/'.$val->id.'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Xem chi tiết">
                    <i class="fa fa-book"></i></a>
                    
                    <a href="admin/bill/edit/'.$val->id.'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Chỉnh sửa">
                    <i class="la la-edit"></i></a>
                    <a id="del-bill-'.$val->id.'" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Xóa">
                        <i class="la la-trash"></i>
                    </a>';
                })
                ->editColumn('id', function ($val) {
                    return '<p>'.$val->id.'</p>';
                })
                ->editColumn('id_customer', function ($val) {
                    return '<p>'.$val->id_customer.'</p>';
                })
            
                ->editColumn('username', function ($val) {
                    return '<p>'.$val->username.'</p>';                
                })
                ->editColumn('phone', function ($val) {
                    return '<p>'.$val->phone.'</p>';                
                })
                ->editColumn('address', function ($val) {
                    return '<p>'.$val->address.'</p>';                
                })
                ->editColumn('date_order', function ($val) {
                    return '<p>'.$val->date_order.'</p>'; 
                })
                
                ->editColumn('total', function ($val) {
                    return '<p>'.$val->total.'</p>'; 
                })

                ->editColumn('payment', function ($val) {
                    return '<p>'.$val->payment.'</p>'; 
                })
                ->editColumn('note', function ($val) {
                    return '<p>'.$val->note.'</p>'; 
                })
                ->rawColumns(['manipulation', 'id', 'id_customer', 'username','phone','address','date_order', 'total','payment', 'note'  ])
                ->make(true);
                break;


            case 'cus':
                return Datatables::of(customers::query())
                ->addColumn('manipulation', function ($val) {
                    return '
                    <a href="admin/customer/edit/'.$val->id.'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Xem chi tiết">
                    <i class="fa fa-book"></i></a>
                    
                    <a href="admin/customer/edit/'.$val->id.'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Chỉnh sửa">
                    <i class="la la-edit"></i></a>
                    <a id="del-cus-'.$val->id.'" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Xóa">
                        <i class="la la-trash"></i>
                    </a>';
                })
                ->editColumn('id', function ($val) {
                    return '<p>'.$val->id.'</p>';
                })
                ->editColumn('username', function ($val) {
                    return '<p>'.$val->username.'</p>';
                })
            
                ->editColumn('email', function ($val) {
                    return '<p>'.$val->email.'</p>';                
                })
            
                ->rawColumns(['manipulation', 'id', 'username','email' ])
                ->make(true);
                break;
            case 'detail':
                return Datatables::of(bill_detail::query())
                ->editColumn('id', function ($val) {
                    return '<p>'.$val->id.'</p>';
                })
                ->editColumn('id_bill', function ($val) {
                    return '<p>'.$val->id_bill.'</p>';
                })

                ->editColumn('id_product', function ($val) {
                    return '<p>'.$val->id_product.'</p>';
                })

                ->editColumn('name_pro', function ($val) {
                    $pro = products::find($val->id_product);
                    return '<p>'.$pro->name.'</p>';
                })
            
                ->editColumn('quantity', function ($val) {
                    return '<p>'.$val->quantity.'</p>';
                })
            
                ->editColumn('unit_price', function ($val) {
                    return '<p>'.number_format($val->unit_price).'</p>';
                })
            
                ->editColumn('total_price', function ($val) {
                    return '<p>'.number_format($val->total_price).'</p>';                
                })
            
                ->rawColumns(['id', 'id_bill','name_pro','email','id_product', 'quantity', 'unit_price', 'total_price'])
                ->make(true);
                break;
            case 'cate':
                return Datatables::of(category::query())
                ->addColumn('manipulation', function ($val) {
                    return '
                    <a href="admin/category/edit/'.$val->id.'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Xem chi tiết">
                    <i class="fa fa-book"></i></a>
                    
                    <a href="admin/category/edit/'.$val->id.'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Chỉnh sửa">
                    <i class="la la-edit"></i></a>
                    <a id="del-cate-'.$val->id.'" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Xóa">
                        <i class="la la-trash"></i>
                    </a>';
                })
                ->editColumn('id', function ($val) {
                    return '<p>'.$val->id.'</p>';
                })
                ->editColumn('name', function ($val) {
                    return '<p>'.$val->name.'</p>';
                })
                ->editColumn('active', function ($val) {
                    return '<p>'.$val->active.'</p>';
                })
            
                ->rawColumns(['id', 'name','active','manipulation' ])
                ->make(true);
                break;

            case 'user':
                return Datatables::of(user::query())
                ->addColumn('manipulation', function ($val) {
                    return '
                    <a href="admin/customer/edit/'.$val->id.'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Xem chi tiết">
                    <i class="fa fa-book"></i></a>
                    
                    <a href="admin/customer/edit/'.$val->id.'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Chỉnh sửa">
                    <i class="la la-edit"></i></a>
                    <a id="del-user-'.$val->id.'" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Xóa">
                        <i class="la la-trash"></i>
                    </a>';
                })
                ->editColumn('id', function ($val) {
                    return '<p>'.$val->id.'</p>';
                })
                ->editColumn('name', function ($val) {
                    return '<p>'.$val->name.'</p>';
                })
                ->editColumn('email', function ($val) {
                    return '<p>'.$val->email.'</p>';
                })

                ->editColumn('position', function ($val) {
                    return '<p>'.$val->position.'</p>';
                })
            
                ->rawColumns(['id', 'name','email','manipulation', 'position' ])
                ->make(true);
                break;


            case 'cmt':
                return Datatables::of(comment::query())
                ->addColumn('manipulation', function ($val) {
                    return '
                    <a id="del-cmt-'.$val->id.'" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Xóa">
                        <i class="la la-trash"></i>
                    </a>';
                })
                ->editColumn('id', function ($val) {
                    return '<p>'.$val->id.'</p>';
                })
                ->editColumn('id_cus', function ($val) {
                    return '<p>'.$val->id_cus.'</p>';
                })
                ->editColumn('id_pro', function ($val) {
                    return '<p>'.$val->id_pro.'</p>';
                })

                ->editColumn('content', function ($val) {
                    return '<p>'.$val->content.'</p>';
                })
            
                ->rawColumns(['id', 'id_cus','id_pro','manipulation', 'content' ])
                ->make(true);
                break;
            default:
                break;
        }
       
    }

    public function getAjaxDel(Request $request, $type) {
        switch ($type) {
            case 'products':
                $id = $request->get('id');
                products::where('id',$id)->delete();
                return response([
                    'status' => true,
                ]);
                break;
            case 'bills':
                $id = $request->get('id');
                bill::where('id',$id)->delete();
                return response([
                    'status' => true,
                ]);
                break;
            
            case 'cate':
                $id = $request->get('id');
                category::where('id',$id)->delete();
                return response([
                    'status' => true,
                ]);
                break;

            case 'user':
                $id = $request->get('id');
                user::where('id',$id)->delete();
                return response([
                    'status' => true,
                ]);
                break;
            case 'cus':
                $id = $request->get('id');
                customers::where('id',$id)->delete();
                return response([
                    'status' => true,
                ]);
                break;
            case 'cmt':
                $id = $request->get('id');
                comment::where('id',$id)->delete();
                return response([
                    'status' => true,
                ]);
                break;
            default:
                break;
        }
       
    }
    public function getAddProduct() {
        if(Auth::user()->position == "Nhân viên") {
            Auth::logout();
            return redirect()->to('admin/login');
        } else {
            $cate = category::all();
            return view('admin.pages.products.add', compact('cate'));
        }
        
    }

    public function getEditProduct($id) {
        if(Auth::user()->position == "Nhân viên") {
            Auth::logout();
            return redirect()->to('admin/login');
        } else {
            $pro = products::find($id);
            $cate = category::all();
            if(!$pro) {
                return view('error');
            } 
            return view('admin.pages.products.edit', compact('pro', 'cate'));
        }
       
    }

    public function postProduct(request $req) {
        $pro = new products;

        $messages = [
		    'required' => ':Attribute require',
            'mimes'  => 'File is not valid',
        ];
        
        $validator = Validator::make($req->all(), [
            'image' => 'required| mimes:jpeg,jpg,png,gif',
            'unit_price'     => 'required',
            'name'     => 'required',
            'quantity'     => 'required',
            'description'     => 'required',

        ], $messages);

        switch ($req->input('action')) {
            case 'add':
                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator);
                } else {
                    $file = $req->file('image');
                    $file_name = $file -> getClientOriginalName('image');
                    $file->move('upload/product', $file_name);
                    $pro->image = $file_name;
                    $pro->active = $req->active;
                    $pro->id_cate = $req->input('id_cate');
                    $pro->name = $req->name;
                    $pro->unit_price = $req->unit_price;
                    $pro->promotion_price = $req->promotion_price;
                    $pro->quantity = $req->quantity;
                    $pro->description = $req->description;

                    $pro->save();
                    return redirect('admin/product')->with('success','Thêm mới thành công');
                }
                break;

            case 'cancel':
                return redirect('admin/product');
                break;
        }

       
    }


    public function updateProduct(Request $req,$id) {
        $pro = products::find($id);

        $messages = [
		    'required' => ':Attribute require',
            'mimes'  => 'File is not valid',
        ];
        
        $validator = Validator::make($req->all(), [
            'image' => ' mimes:jpeg,jpg,png,gif',
            'unit_price'     => 'required',
            'name'     => 'required',
            'promotion_price'     => 'required',
            'quantity'     => 'required',
            'description'     => 'required',
        ], $messages);

        switch ($req->input('action')) {
            case 'save':
                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator);
                } else {
                    if( $req -> hasFile('image')) {
                        $file = $req->file('image');
                        $file_name = $file -> getClientOriginalName('image');
                        $file->move('upload/product', $file_name);
                        $pro->image = $file_name;
                        $pro->active = $req->active;
                        $pro->id_cate = $req->input('id_cate');
                        $pro->name = $req->name;
                        $pro->unit_price = $req->unit_price;
                        $pro->promotion_price = $req->promotion_price;
                        $pro->quantity = $req->quantity;
                        $pro->description = $req->description;
                        $pro->save();
                        return redirect('admin/product')->with('success','cập nhật thành công');
                    } else {
                        $pro->image =  $pro->image;
                        $pro->active = $req->active;
                        $pro->id_cate = $req->input('id_cate');
                        $pro->name = $req->name;
                        $pro->unit_price = $req->unit_price;
                        $pro->promotion_price = $req->promotion_price;
                        $pro->quantity = $req->quantity;
                        $pro->description = $req->description;
                        $pro->save();
                        return redirect('admin/product')->with('success','cập nhật thành công');
                    }
                }
                break;

            case 'cancel':
                return redirect('admin/product');
                break;
        }
    }


    public function deleteProduct($id) {
        $pro = products::find($id);
        $pro->delete();
        return redirect() -> back(); 
    }

    // public function search(Request $req) {
    //     $k=$req->key;
    //     $product=products::where('name','LIKE','%'.$k.'%')->get();
    //     $cate = category::all();
    //     return response([
    //         'product'=>$product,
    //         'cate'=>$cate,
    //     ]);
    // }




    

    // customer
    public function getCustomer() {
        if(Auth::user()->position == "Nhân viên") {
            Auth::logout();
            return redirect()->to('admin/login');
        } else {
            $cus = customers::all();
            $stt = 1;
            return view('admin.pages.customer.customer', compact('cus', 'stt'));
        }
       
    }

    public function getAddCustomer() {
        if(Auth::user()->position == "Nhân viên") {
            Auth::logout();
            return redirect()->to('admin/login');
        } else {
            return view('admin.pages.customer.add');
        }
        
    }

    public function getEditCustomer($id) {
        if(Auth::user()->position == "Nhân viên") {
            Auth::logout();
            return redirect()->to('admin/login');
        } else {
            $cus = customers::find($id);
            return view('admin.pages.customer.edit', compact('cus'));
        }
       
    }

    public function postCus(request $req) {
        $cus = new customers;
        $messages = [
            'name.required' => ':Attribute require',
            'email' => 'Email is not valid',
            'unique' => 'Email had exist',
        ];
        
        $validator = Validator::make($req->all(), [
            'name'     => 'required',
            'email'     => 'required|email|unique:customers',
            'password' => 'required',
        ], $messages);

        switch ($req->input('action')) {
            case 'add':
                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator);
                } else {
                    $cus->email = $req->email;
                    $cus->username = $req->name;
                    $cus ->password = Hash::make($req->password);
                    $cus->save();
                    return redirect('admin/customer')->with('success','Thêm mới thành công');
                }
                break;
            case 'cancel':
                return redirect('admin/customer');
                break;
        }
    }


    public function updateCus(request $req,$id) {
        $cus = customers::find($id);

        $messages = [
		    'name' => ':Attribute require',
            'unique' => 'Email had exist',
        ];
        
        $validator = Validator::make($req->all(), [
            'name'     => 'required',
            'email'     => 'email|required|unique:customers,email,'.$cus->id,
        ], $messages);

        switch ($req->input('action')) {
            case 'edit':
                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator);
                } else {
                    $cus->username = $req ->name;
                    $cus->email = $req->email;
                    $cus->save();
                    return redirect('admin/customer')->with('success','cập nhật thành công');
                }
                break;
            case 'cancel':
                return redirect('admin/customer');
                break;
        }
    }

    public function deleteCus($id) {
        $cus = customers::find($id);
        $cus->delete();
        return redirect() -> back(); 
    }



    

    // user
    public function getUser() {
        if(Auth::user()->position == "Nhân viên") {
            Auth::logout();
            return redirect()->to('admin/login');
        } else {
            $user = User::all();
            $stt = 1;
            return view('admin.pages.users.users', compact('user', 'stt'));
        }
        
    }

    public function getAddUser() {
        if(Auth::user()->position == "Nhân viên") {
            Auth::logout();
            return redirect()->to('admin/login');
        } else {
            return view('admin.pages.users.add');
        }
       
    }

    public function getEditUser($id) {
        if(Auth::user()->position == "Nhân viên") {
            Auth::logout();
            return redirect()->to('admin/login');
        } else {
            $user = User::find($id);
            return view('admin.pages.users.edit', compact('user'));
        }
        
    }

    public function postUser(request $req) {
        $user = new User;
        $messages = [
            'name.required' => ':Attribute require',
            'email' => 'Email is not valid',
            'unique' => 'Email had exist',
        ];
        
        $validator = Validator::make($req->all(), [
            'name'     => 'required',
            'email'     => 'required|email|unique:users',
            'password' => 'required',
        ], $messages);

        switch ($req->input('action')) {
            case 'add':
                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator);
                } else {
                    $user->email = $req->email;
                    $user->name = $req->name;
                    $user->position = $req->position;
                    $user ->password = Hash::make($req->password);
                    $user->save();
                    return redirect('admin/user')->with('success','Thêm mới thành công');
                }
                break;
            case 'cancel':
                return redirect('admin/user');
                break;
        }
    }

    public function updateUser(Request $req, $id) {
        $user = User::find($id);

        $messages = [
		    'name.required' => ':Attribute require',
            'email' => 'Email is not valid',
            'unique' => 'Email had exist',
        ];
        
        $validator = Validator::make($req->all(), [
            'name'     => 'required',
            'email'     => 'required|email|unique:users,email,'.$user->id,
        ], $messages);

        switch ($req->input('action')) {
            case 'edit':
                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator);
                } else {
                    
                    $user->name = $req ->name;
                    $user->email = $req->email;
                    $user->save();
                    return redirect('admin/user')->with('success','cập nhật thành công');
                }
                break;
            case 'cancel':
                return redirect('admin/user');
                break;
        }
    }

    // bill
    public function bill() {
        $customer = customers::all();
        $bill = bill::paginate(10);
        $bill_count = bill::all()->count();
        $stt = 1;
        return view('admin.pages.bills.bills', compact('bill', 'stt', 'bill_count', 'customer'));
    }

    public function getEditBill($id) {
        $bill = bill::find($id);
        return view('admin.pages.bills.edit', compact('bill'));
    }

    public function updateBill(Request $req, $id) {
        $bill = bill::find($id);

        $messages = [
		    'required' => ':Attribute require',
        ];
        
        $validator = Validator::make($req->all(), [
            'name'     => 'required',
            'phone'     => 'required',
            'address'     => 'required',
        ], $messages);

        switch ($req->input('action')) {
            case 'save':
                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator);
                } else {
                    $bill->payment = $req->payment;
                    $bill->username = $req->name;
                    $bill->phone = $req->phone;
                    $bill->address = $req->address;
                    $bill->note = $req->note;
                    $bill->save();
                    return redirect('admin/bill')->with('success','cập nhật thành công');
                }
                break;
            case 'cancel':
                return redirect('admin/bill');
                break;
        }
    }

    public function billDetails($id) {
        $bill_detail = bill_detail::where('id_bill', '=', $id)->get();
        $stt = 1;
        $pro = products::all();
        return view('admin.pages.bills.detail', compact('bill_detail', 'stt','pro'));
    }


    // bill-detail
    public function billdetail() {
        $bill_detail = bill_detail::paginate(10);
        $detail_count = bill_detail::all()->count();
        $pro = products::all();
        $stt = 1;
        return view('admin.pages.bill-detail.bill-detail', compact('bill_detail', 'stt', 'detail_count', 'pro'));
    }

    public function deleteBilldetail($id) {
        $bill_detail = bill_detail::find($id);
        $bill_detail->delete();
        return redirect() -> back(); 
    }


    // comment
    public function comment() {
        $comment = comment::paginate(10);
        $cmt_count = comment::all()->count();
        $pro = products::all();
        $cus = customers::all();
        $stt = 1;
        return view('admin.pages.comments.comment', compact('comment', 'stt', 'cmt_count', 'pro', 'cus'));
    }
}
