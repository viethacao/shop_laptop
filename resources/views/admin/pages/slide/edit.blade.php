@extends('admin/master')
@section('title')
    edit slide
@endsection
@section('content')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader ">
            <div class="m-portlet__body">
                <div class="row">
                    <div class="col-7">
                        <div class="m-portlet m-portlet--tab">
                            <div class="m-portlet__head">
                                <div class="m-portlet__head-caption">
                                    <div class="m-portlet__head-title">
                                        <span class="m-portlet__head-icon m--hide">
                                            <i class="la la-gear"></i>
                                        </span>
                                        <h3 class="m-portlet__head-text">
                                            Edit Slide
                                        </h3>
                                    </div>
                                </div>
                            </div>
                            <!--begin::Form-->
                            <form class="m-form m-form--fit m-form--label-align-right" action="{{route('update-slide', $slide->id)}}" method="post" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                @foreach ($errors->all() as $error)
                                    <div class="alert alert-danger">{{ $error }}</div>
                                @endforeach
                                <div class="m-portlet__body">
                                    
                                    <div class="form-group m-form__group">
                                        <label for="exampleInputEmail1">
                                            Image
                                        </label>
                                        <input onchange="changeImage(this)" name="image" type="file" class="form-control m-input m-input--square">
                                        <img class="image-change" src="upload/slide/{{$slide->image}}" />
                                    </div>
                                    <div class="form-group m-form__group">
                                        <label for="exampleInputEmail1">
                                            Link
                                        </label>
                                        <input value="{{$slide->link}}" name="link" type="text" class="form-control m-input m-input--square"  placeholder="Enter link image">
                                       
                                    </div>
                                    <div class="form-group m-form__group">
                                        <label for="exampleSelect1">
                                            Active
                                        </label>
                                        <select name="active" class="form-control m-input m-input--square">
                                            <option style="display:none"  selected>{{$slide->active}}</option>
                                            <option>
                                                Active
                                            </option>
                                            <option>
                                                Inactive
                                            </option>
                                            
                                        </select>
                                    </div>
                                </div>
                                <div class="m-portlet__foot m-portlet__foot--fit">
                                    <div class="m-form__actions">
                                        <button value="edit" name="action" type="submit" class="btn btn-metal">
                                            Edit
                                        </button>
                                        <button value="cancel" name="action" type="submit" class="btn btn-secondary">
                                            Cancel
                                        </button>
                                    </div>
                                </div>
                            </form>
                            <!--end::Form-->
                        </div>
                    </div>
                </div>
            </div>
                
        </div>
        <!-- END: Subheader -->
       
        

    </div>



    
@endsection