@extends('admin/master')


@section('title')
    home
@endsection
@section('content')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader ">
            <!--begin::Portlet-->
            <div class="m-portlet">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Thống kê
                            </h3>
                        </div>
                    </div>
                </div>
               
                <div class="m-portlet__body">
                    <!--begin::Section-->
                    <div class="m-section">
                        <div class="row">
                            <div class="col-4">
                                <div class="m-section__content" style="margin-bottom: 41px;padding: 10px;text-align: center;border: 1px solid #ccc;box-shadow: 3px 4px 5px #ccc;">
                                    <h6 style="font-weight: bold; font-size: 16px;">Tổng danh mục sản phẩm</h6>
                                    <p style="font-size: 27px; margin-bottom: 0px; color: #1d2dad;">
                                            {{$cate}} danh mục
                                    </p>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="m-section__content" style="margin-bottom: 41px;padding: 10px;text-align: center;border: 1px solid #ccc;box-shadow: 3px 4px 5px #ccc;">
                                    <h6 style="font-weight: bold; font-size: 16px;">Tổng sản phẩm</h6>
                                    <p style="font-size: 27px; margin-bottom: 0px; color: #1d2dad;">
                                            {{$pro}} sản phẩm
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            
                            <div class="col-4">
                                <div class="m-section__content" style="margin-bottom: 41px;padding: 10px;text-align: center;border: 1px solid #ccc;box-shadow: 3px 4px 5px #ccc;">
                                    <h6 style="font-weight: bold; font-size: 16px;">Tổng sản phẩm còn lại trong kho</h6>
                                    <p style="font-size: 27px; margin-bottom: 0px; color: #1d2dad;">
                                            {{$store}} sản phẩm
                                    </p>
                                </div>
                            </div>

                            <div class="col-4">
                                <div class="m-section__content" style="margin-bottom: 41px;padding: 10px;text-align: center;border: 1px solid #ccc;box-shadow: 3px 4px 5px #ccc;">
                                    <h6 style="font-weight: bold; font-size: 16px;">Tổng sản phẩm đã bán</h6>
                                    <p style="font-size: 27px; margin-bottom: 0px; color: #1d2dad;">
                                            {{$sum2}} sản phẩm
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-4">
                                <div class="m-section__content" style="margin-bottom: 41px;padding: 10px;text-align: center;border: 1px solid #ccc;box-shadow: 3px 4px 5px #ccc;">
                                    <h6 style="font-weight: bold; font-size: 16px;">Tổng số hóa đơn</h6>
                                    <p style="font-size: 27px; margin-bottom: 0px; color: #1d2dad;">
                                            {{$bill}} hóa đơn
                                    </p>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="m-section__content" style="margin-bottom: 41px;padding: 10px;text-align: center;border: 1px solid #ccc;box-shadow: 3px 4px 5px #ccc;">
                                    <h6 style="font-weight: bold; font-size: 16px;">Tổng số chi tiết hóa đơn</h6>
                                    <p style="font-size: 27px; margin-bottom: 0px; color: #1d2dad;">
                                            {{$detail}} chi tiết hóa đơn
                                    </p>
                                </div>
                            </div>
                        </div>
                            <div class="col-4">
                                <div class="m-section__content" style="margin-bottom: 41px;padding: 10px;text-align: center;border: 1px solid #ccc;box-shadow: 3px 4px 5px #ccc;">
                                    <h6 style="font-weight: bold; font-size: 16px;">Tổng số khách hàng</h6>
                                    <p style="font-size: 27px; margin-bottom: 0px; color: #1d2dad;">
                                            {{$cus}} khách hàng
                                    </p>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="m-section__content" style="margin-bottom: 41px;padding: 10px;text-align: center;border: 1px solid #ccc;box-shadow: 3px 4px 5px #ccc;">
                                    <h6 style="font-weight: bold; font-size: 16px;">Tổng số banner quảng cáo</h6>
                                    <p style="font-size: 27px; margin-bottom: 0px; color: #1d2dad;">
                                            {{$slide}} banner
                                    </p>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="m-section__content" style="margin-bottom: 41px;padding: 10px;text-align: center;border: 1px solid #ccc;box-shadow: 3px 4px 5px #ccc;">
                                    <h6 style="font-weight: bold; font-size: 16px;">Tổng số bình luận</h6>
                                    <p style="font-size: 27px; margin-bottom: 0px; color: #1d2dad;">
                                            {{$cmt}} bình luận
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end::Section-->
                </div>
                <!--end::Form-->
            </div>
            <!--end::Portlet-->
                
        </div>
        <!-- END: Subheader -->
       
        

    </div>
@endsection